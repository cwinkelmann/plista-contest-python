from distutils.core import setup
from setuptools import find_packages

setup(
    name='plistaContestPy',
    version='1.0.0',
    author='Christian Winkelmann',
    author_email='christian.winkelmann@gmail.com',
    packages=find_packages(),
    license='LICENSE.txt',
    description='package for the plista contest',
    long_description=open('README.txt').read(),
    install_requires=[
        "redis", "numpy", "flask", "pika", "mysql-python", "pandas", 'numba'
        ],
)