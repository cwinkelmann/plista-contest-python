== build ==
python setup.py sdist


== deploy ==
$ sudo apt-get install python-dev python-pip python-virtualenv ipython python-nose gfortran python-dev libatlas3gf-base libblas-dev libatlas-base-dev liblapack3gf gfortran gfortran-multilib g++  libatlas3gf-base myssql-server mysql-client libmysqlclient-dev

redis is needed in version 2.6 so grab it from the homepage and build it

scp -i ~/Documents/contest-py.pem plistaContestPy/dist/plistaContesPy-1.0.0.tar.gz ubuntu@ec2-46-137-1-77.eu-west-1.compute.amazonaws.com:/home/ubuntu
ssh -i ~/Downloads/contest_1.pem ubuntu@ec2-46-137-1-77.eu-west-1.compute.amazonaws.com

$ virtualenv plista-contest
and now you have to source the environment
$ . env/bin/activate

export PYTHONPATH=/..path to env../dist/virtual_env/lib/python2.7/site-packages/contest


== run ==

on the server, execute
./scripts/build.sh
sudo pip install -U plista-contest-python-1.0.0.tar.gz;
$ python /home/ubuntu/plista-contest/lib/python2.7/site-packages/plistaContestPy/