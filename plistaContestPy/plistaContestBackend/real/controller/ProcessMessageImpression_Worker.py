'''
Created on 24.08.2012



@author: christian.winkelmann@plista.com
'''
import logging
from plistaContestPy.plistaContestBackend.config import config_local
from plistaContestPy.plistaContestBackend.real.packages.message_parsers.MessageParser import MessageParser
import pika
import cPickle
from plistaContestPy.plistaContestBackend.real.packages.recommenders.DomainBasedRandomRecommender import DomainBasedRandomRecommender

from plistaContestPy.plistaContestBackend.real.packages.recommenders.MyMediaLiteWrapper import MyMediaLiteWrapper
from plistaContestPy.plistaContestBackend.real.packages.recommenders.Random_Recommender import Random_Recommender


class ProcessMessageImpression_Worker(object):
    queue_name = "process" #todo config
    routing_key = "process" #todo config

    def __init__(self, runner = False):
        if runner:
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(
                host='localhost')) # todo this needs to be configured via a config
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue=self.queue_name)


    def main(self):
        """ main worker function """

        print ' [*] Waiting for messages. To exit press CTRL+C'

        self.channel.basic_consume(self.callback,
                                   queue=self.queue_name)
        self.channel.start_consuming()


    def callback(self, ch, method, properties, body):
        """ in the body is metadata and the main information """
        try:
            _body = cPickle.loads(body)
            message_instance = _body['message']

            #message_instance = cPickle.loads(message_instance)
            metadata = _body['metadata']
            self.process_message(message_instance)
            print "body: {}".format(_body)


            ch.basic_ack(delivery_tag = method.delivery_tag) # send an ack
        except:
            print "something went wrong here: {}".format(body)


    def enqueue(self, metadata, message):
        print message
        body = {"metadata":metadata, "message":message}
        _body = cPickle.dumps(body)
        self.channel.basic_publish(exchange='',
                                   routing_key=self.routing_key,
                                   body=_body)



    def process_message(self, message_instance):
        """
        Process the messages ASYNC
        @type message_instance: MessageParser
        @param message_instance:
        @return:
        """
        self.writeBack(message_instance)
        self.triggerRecommendation(message_instance)




    def log_data(self, message):
        """
            @type message: MessageParser
            @param message: a general message.
        """
        # TODO write data to log files
        # TODO write statistics
        logger = logging.getLogger("processMessage:")

        logger.info(message)

    def triggerRecommendation(self, message_instance):
        """

        @param message_instance:
        @return:
        """
        self.train_recommender(message_instance)

    def train_recommender(self, message):
        """
            @type message: MessageParser
            @param message: a general message.
        """
        """
        rrW = Random_Recommender()
        if message.item_recommendable or 1:
            rrW.set_recommendables( message.item_id, { 'domainid' : message.domain_id } )
        rrW.train(message.user_id, { 'domainid' : message.domain_id })
        """
        dBR = DomainBasedRandomRecommender()
        dBR.set_recommendables( item_id = message.item_id, domain_id =  message.domain_id, recommendable = message.item_recommendable )

        # TODO implement the mymediaLite Wrapper
        """
        mML = MyMediaLiteWrapper()
        mML.set_recommendables( message.item_id, { 'domainid' : message.domain_id } )
        mML.train(message.user_id, { 'domainid' : message.domain_id })
        """

    def writeBack(self, message_instance):
        """
        @param message_instance: the message
        @type message_instance: MessageParser
        @return: None
        """
        #print "writeBack: write the data to somewhere"
        # todo implement this
        pass

if __name__ == '__main__':
    rw = ProcessMessageImpression_Worker(config_local.messaging_debug)
    rw.main()