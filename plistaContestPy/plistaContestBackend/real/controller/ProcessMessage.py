'''
Created on 24.08.2012



@author: christian.winkelmann@plista.com
'''
from plistaContestPy.plistaContestBackend.config import config_local

from ProcessMessage_Feedback import ProcessMessage_Feedback
from ProcessMessage_Impression import ProcessMessage_Impression

from plistaContestPy.plistaContestBackend.real.packages.message_parsers.MessageParser import MessageParser
from constants import *
from constants import MESSAGE_TYPE_IMPRESSION
from plistaContestPy.plistaContestBackend.real.packages.helper import bootstrap

import json

class ProcessMessage(object):

    message = ""
    results = []

    def __init__(self, message):
        """ @param: message a contest message which has to be parsed and processed
        @type message: str
        @return : list
        """

        #bootstrap.bootstrap()
        #self.processMessageLogger = bootstrap.getLogger("ProcessMessage")


        message_instance = MessageParser() # instance of a message parser
        message_instance.parse(message) # parse the message
        #self.processMessageLogger.info("INCOMING: " + message)
        self.message_instance = message_instance


        """ if the message is an Impression """
        if message_instance.message_type == MESSAGE_TYPE_IMPRESSION:
            pI = ProcessMessage_Impression(message_instance)
            self.results = pI.result
        elif message_instance.message_type == MESSAGE_TYPE_FEEDBACK:
            pI = ProcessMessage_Feedback(message_instance)

        """ if the message is an Feedback """


    def compose_result_message(self):
        """ the result has to be somehting like this
        '{"msg":"result","items":[{"id":48457814},{"id":48387562},{"id":48411046},{"id":48333490}],"team":{"id":"15"},"version":"1.0"}'
        """
        items = []
        for i in self.results:
            items.append({"id":i})

        message = {"msg":"result",
                   "items": items,
                   "team":{"id":config_local.team_id},
                   "version":config_local.api_version
        }
        message_json = json.dumps(message)
        #self.processMessageLogger.info("OUTGOING: " + message_json)
        return message_json



