from plistaContestPy.plistaContestBackend.config import config_local
from plistaContestPy.plistaContestBackend.real.controller.ProcessMessageImpression_Worker import ProcessMessageImpression_Worker
from plistaContestPy.plistaContestBackend.real.packages.message_parsers.MessageParser import MessageParser
from plistaContestPy.plistaContestBackend.real.controller.constants import *
from plistaContestPy.plistaContestBackend.real.packages.recommenders.DomainBasedRandomRecommender import DomainBasedRandomRecommender
from plistaContestPy.plistaContestBackend.real.packages.recommenders.Random_Recommender import *


class ProcessMessage_Impression(object):
    """
    if the incoming message is an Impression do the following
    """
    message = ""
    result = []
    RANDOM_RECOMMENDER_ENABLED = False
    DOMAIN_BASED_RECOMMENDER_ENABLED = True

    def __init__(self, message_instance):
        """
            @type message_instance: MessageParser
            @param message_instance: a general message.
        """
        self.message_instance = message_instance

        """ always trigger a training """
        debug = config_local.messaging_debug
        if not debug: # we are in live now
            pMW = ProcessMessageImpression_Worker()
            pMW.enqueue(metadata={}, message=message_instance)

        elif debug:
            pMW = ProcessMessageImpression_Worker()
            pMW.process_message(message_instance)

        # anything else will be done async
        #if message_instance.item_recommendable:
        #    self.store_recommendable_item(message_instance) # TODO fix this, this is redundant to the ProcessMessage

        if message_instance.config_recommend:
            self.result = self.recommend(message_instance)






#    def __str__(self):
#        return "bla"

    def recommend(self, message):
        """ now the actual Recommendation is done
            @type message: MessageParser
            @param message: a general message.
        """

        if self.RANDOM_RECOMMENDER_ENABLED:
            fb = Random_Recommender()
            domain_id = message.domain_id
            user_id = message.user_id
            limit = message.config_limit
            result = fb.get_recommendation(user_id, { 'domainid' : domain_id }, limit, remove=True, ranked=False )

        if self.DOMAIN_BASED_RECOMMENDER_ENABLED:
            db = DomainBasedRandomRecommender()
            result = db.get_recommendation(domain_id = message.domain_id, limit = message.config_limit)
        # TODO start mixing the results

        return result


    def store_recommendable_item(self, message):
        """ if the item is recommendable we need to do something with it
        @type message: MessageParser
        @param message: a general message.
        """



        if self.RANDOM_RECOMMENDER_ENABLED:
            fb = Random_Recommender()

            item_id = message.item_id
            domain_id = message.domain_id
            fb.set_recommendables( item_id, { 'domainid' : domain_id } )
        