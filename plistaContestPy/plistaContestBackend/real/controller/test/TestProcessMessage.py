import unittest
import json
import redis
from contest.controller.ProcessMessage import *
from contest.IntegrationTests.contestReplay.APIMessageGenerator import APIMessageGenerator

class TestProcessMessage(unittest.TestCase):
    def setUp(self):
        """
        """
        self.redis_con = redis.Redis("localhost")
        self.redis_con.flushall()
        self.apiMessageGenerator = APIMessageGenerator()


    def tearDown(self):
        """
        """

    def __import_impressions(self, number_of_impression = 200, starting_id = 1, recommend_flag = False):
        for i in range(starting_id,number_of_impression):
            message = self.apiMessageGenerator.get_custom_impression_message(
                id_flag = i, # autoincrement message ids
                client_id_flag = i % 3, # three users are making the impression
                item_id_flag = i % 6,
                recommendable_flag = i % 2,
                recommend_flag = recommend_flag,
                message_json = True,
                domain_id = 1
            )
            ProcessMessage(message)

    def __import_feedback(self, domain_id, item_id_list, client_id):
        """

        @param message_id:
        @param domain_id:
        @param item_id_list:
        @type item_id_list: list
        @return:
        """
        from random import randint
        item_id_flag = item_id_list[randint(0,len(item_id_list)-1)]
        message = self.apiMessageGenerator.get_custom_feedback_message(domain_id=domain_id, item_id_flag=item_id_flag, client_id=client_id)
        ProcessMessage(message)


    def __recommend_message(self):
        """ @return pm: Process message after calculating a result for a "recommend" message
        """
        message = self.apiMessageGenerator.get_custom_impression_message(
            id_flag = 21,
            client_id_flag = 1, # three users are making the impression
            item_id_flag = 1,
            recommendable_flag = False,
            recommend_flag = True,
            message_json = True,
            domain_id=1
        )

        pm = ProcessMessage(message)
        return pm


    def test_small_Integration_async(self):
        """
        send 10 impressions without any recommendation request
        send 1 impression with a recommendation request

        the result has to be non empty and with items among the first if they were recommendable
        """
        config_local.messaging_debug = False
        self.small_integration()


    def test_small_Integration_sync(self):
        """
        send 10 impressions without any recommendation request
        send 1 impression with a recommendation request

        the result has to be non empty and with items among the first if they were recommendable
        """
        config_local.messaging_debug = True
        self.small_integration()

    def __test_Big_Integration_sync(self):
        """
        synchronous integration with impressions, feedbacks...
        the result has to be non empty and with items among the first if they were recommendable
        """
        config_local.messaging_debug = True
        self.big_integration()



    def __big_integration(self):
        # FIXME broken test
        number_of_impression = 200
        self.__import_impressions(number_of_impression = number_of_impression, starting_id = 1)
        pm = self.__recommend_message()
        result_message = pm.compose_result_message()
        print pm.results


        self.__import_feedback(domain_id = pm.message_instance.domain_id,
            item_id_list=pm.results,
            client_id=pm.message_instance.user_id)

        #self.__import_impressions(number_of_impression = 2 + 2*number_of_impression, starting_id = number_of_impression + 2)
        #pm = self.__recommend_message()

        self.assertEquals(4, len(pm.results))
        self.assertEquals(type([]), type(pm.results), "the result message has the wrong type")


    def small_integration(self):
        self.__import_impressions(number_of_impression = 10)
        pm = self.__recommend_message()
        pm.compose_result_message()
        print pm.results
        self.assertEquals(4, len(pm.results))
        self.assertEquals(type([]), type(pm.results), "the result message has the wrong type")


    def _test_compose_result_message(self):
        """ test if composing a result works
        """
        self.__import_impressions(number_of_impression=4)
        pm = self.__recommend_message()

        result_message = pm.compose_result_message()

        self.assertEqual(type(result_message), type("string"))
        converted_message = json.loads(result_message)
        self.assertEqual("result", converted_message["msg"])

        self.assertGreater(len(converted_message['items']), 0, "there are no items in the recommended list")




#    def get_custom_impression_message(self, id_flag, client_id_flag, item_id_flag, recommendable_flag, recommend_flag, message_json = True):
#        message = {"msg":"impression",
#                   "id":id_flag,
#                   "client":{"id":client_id_flag},
#                   "domain":{"id":"418"},
#                   "item":{"id":item_id_flag,
#                           "title":"The Title Text",
#                           "url":"http://url.de",
#                           "created":"1318417485",
#                           "text":"Mit zehn Siegen ",
#                           "img":None,
#                           "recommendable":recommendable_flag},
#                   "config":{"timeout":None,
#                             "recommend":recommend_flag,
#                             "limit":4},
#                   "version":"1.0"}
#
#        if message_json:
#            return json.dumps(message)
#        else:
#            return message
#
#
#    def get_custom_feedback_message(self, domain_id, item_id_flag, client_id, message_json = True):
#        """
#        @param message_id: the id of message
#        @param domain_id: the domain id
#        @param client_id_flag: the user id
#        @param source_id: the source where the user came from
#        @param item_id_flag:
#        @param message_json:
#        @return:
#        """
#
#        message = {"msg":"feedback",
#                   "client":
#                           {"id":client_id},
#                   "domain":{"id":domain_id},
#                   "source":{"id":"2311"},
#                   "target":{"id": item_id_flag},
#                   "config":{"team":{"id":22}},
#                   "version":"1.0"}
#
#        if message_json:
#            return json.dumps(message)
#        else:
#            return message
#



if __name__ == '__main__':
    unittest.main()
