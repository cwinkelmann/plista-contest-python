__author__ = 'christian'
from TestProcessMessage import TestProcessMessage

import cProfile

tP = TestProcessMessage("test_small_Integration_async")
cProfile.run( "tP.small_integration()", "profile")

import pstats
p = pstats.Stats('profile')


p.sort_stats('cumulative').print_stats(10)