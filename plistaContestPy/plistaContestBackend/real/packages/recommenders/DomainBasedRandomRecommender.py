'''
Created on 23.05.2012

quite basic recommender which bases recommending random items, but exclude items alredy seen and is therefore 
individual for each user. For unknown users other dimensions can be taken like their browser or publisher

@author: christian.winkelmann@plista.com
'''
import logging


from plistaContestPy.plistaContestBackend.real.packages.recommenders.baseRecommender import baseRecommender
from plistaContestPy.plistaContestBackend.real.packages.models.RecommendationList import RecommendationList
import redis
import random

class DomainBasedRandomRecommender(baseRecommender):
    '''
    this will be the most naive recommender which just outputs random recommendations
    '''
    key = None
    userid = None

    def __init__(self):
        super(DomainBasedRandomRecommender, self).__init__()
        self.redis_con = redis.Redis("localhost")


    def compute_key(self, domain_id):
        """ compute the key of this DomainBasedRandomRecommender
        """
        return "DomainBasedRandomRecommender:{}".format(domain_id)


    def get_recommendation(self, domain_id, limit):
        """
        return
        @param domain_id: actual domainid
        @param limit: number of items
        """
        key = self.compute_key(domain_id)
        result = self.redis_con.srandmember(key, number=limit)
        return result



    def train(self, userid, addition_filter, N=10 ):
        """
        """
        pass


    def train_filter(self, N=10, additional_filter=None):
        """ compute a random recommendation
        due to limitations like domain specific articles there should be restrictions

        @param N number of recommendations
        @param additional_filter dictionary to be used
        """
        pass


    def set_recommendables(self, item_id, domain_id, recommendable):
        """
        if the item is recommendable it can be recommended the next time an impression asks for it
        if not, then it should be removed from the list of items
        """
        key = self.compute_key(domain_id)
        if recommendable:
            self.redis_con.sadd(key, item_id)
        else:
            self.redis_con.srem(key, item_id)


    def get_recommendable_item(self, domain_id):
        """

        """
        key = self.compute_key(domain_id)
        return self.redis_con.srandmember(key)

    def get_amount_of_recommendables(self, key ):
        pass



if __name__ == '__main__':
    '''
    '''


