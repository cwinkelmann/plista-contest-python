import unittest
import redis

from plistaContestPy.plistaContestBackend.real.packages.recommenders.DomainBasedRandomRecommender import DomainBasedRandomRecommender


class TestDomainBasedRandomRecommender(unittest.TestCase):

    debug = True

    def setUp(self):
        self.redis_con = redis.Redis("localhost")
        self.redis_con.flushall()



    def tearDown(self):
        pass


    def insertRecommendables(self, domain_id, N1, N2):
        fb = DomainBasedRandomRecommender()
        for item_id in xrange(N1,N2):
            fb.set_recommendables( item_id = item_id, domain_id = domain_id, recommendable=True )


    def test_get_recommendable_item(self):
        fb = DomainBasedRandomRecommender()

        # inserting two items for domain1
        fb.set_recommendables( 1, domain_id= 'domain1', recommendable=True )
        fb.set_recommendables( 2, domain_id = 'domain1', recommendable=True)
        g = fb.get_recommendable_item( domain_id = 'domain1' )

        self.assertIn(int(g), (1,2), "fetched wrong recommendable item")

        # for this domain there is no way of recommending anything, because we don't have any items
        g = fb.get_recommendable_item( { 'domainid' : 'domain2' } )
        self.assertIsNone(g, "fetched wrong recommendable item")





    def test_Get_Recommendation(self):
        for N in xrange(1,9):
            fb = DomainBasedRandomRecommender( )
            self.insertRecommendables("domainid1", 0, 10)
            self.insertRecommendables("domainid2", 20, 30)

            resultSet_1 = fb.get_recommendation( domain_id= "domainid1", limit=N )
            if self.debug: print resultSet_1
            self.assertEqual(len(resultSet_1), N, 'the resulting recommendation have the wrong number')






if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()