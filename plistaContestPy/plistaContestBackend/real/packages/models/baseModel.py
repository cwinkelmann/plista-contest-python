'''
Created on 25.12.2011

@author: christian.winkelmann@plista.com
'''
from plistaContestPy.plistaContestBackend.config import config_global

from plistaContestPy.plistaContestBackend.config import config_local
from plistaContestPy.plistaContestBackend.real.packages.designPatterns import Singleton
from plistaContestPy.plistaContestBackend.config.cassandraConnection import CassandraConnection
from plistaContestPy.plistaContestBackend.config.cassandraConnection import RedisConnection


class baseModel(object):
    '''
    base Model connection
    '''

    def __init__(self, mode = 'cassandra'):
        '''
        Constructor
        '''
        
        if ( mode == 'cassandra' ):
            self.conn = CassandraConnection()
        else:
            if ( mode == 'redis' ):
                #self.conn = redis.Redis("localhost")
				self.conn = RedisConnection() #redis.Redis("localhost")
        
    def save(self):
        """ to overload this function """
        return "now it should be saved"
    
    def get(self):
        """ get something """
        return " I got something "
        
