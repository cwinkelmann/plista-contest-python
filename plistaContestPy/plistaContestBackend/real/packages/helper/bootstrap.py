import logging
import plistaContestPy.plistaContestBackend.config.config_local as config

__author__ = 'christian'


def bootstrap():

    #logging_helper.initLogging()

    logging.basicConfig(level=logging.DEBUG,
        filename='contest-py.log',
        filemode='w')

def getLogger(loggername):
    """
    @param loggername: the name of the logger
    @type loggername: str
    @return:theLogger
    """
    theLogger = logging.getLogger(loggername)
    fh = logging.FileHandler(config.log_file_location + loggername + '.log')
    theLogger.addHandler(fh)
    return theLogger



class contestLogger(object):
    """

    """
