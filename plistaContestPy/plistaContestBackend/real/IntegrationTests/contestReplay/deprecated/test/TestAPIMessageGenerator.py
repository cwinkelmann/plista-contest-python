import unittest
from contest.IntegrationTests.contestReplay.APIMessageGenerator import validItems, APIMessageGenerator

__author__ = 'christian'



class TestValidItems(unittest.TestCase):
    def setUp(self):
        """
        """

    def tearDown(self):
        """
        """

    def testGetValidItems(self):
        vI = validItems()
        vI.setValid(id = 1, type = "domain", type_id = 1)
        items = vI.getValid(type = "domain", type_id = 1)
        self.assertEquals(1, len(items), "one item was added but not retrieved")

        vI.setValid(id = 1, type = "domain", type_id = 1)
        items = vI.getValid(type = "domain", type_id = 1)
        self.assertEquals(1, len(items), "the same item should not get added twice")

        items = vI.getValid(type = "domain", type_id = 2)
        self.assertEquals(0, len(items), "there no valid items at this domain")


        vI.setValid(id = 2, type = "domain", type_id = 1)
        items = vI.getValid(type = "domain", type_id = 1)
        self.assertEquals(2, len(items), "after adding a second item we should get back 2")
        self.assertListEqual(items, [1,2], "wrong items are in the list")

        vI.setValid(id = "c1", type = "country", type_id = 1)
        items = vI.getValid(type = "country", type_id = 1)
        self.assertEquals(1, len(items), "the same item should not get added twice")
        #adding the item again should n

        vI.setValid(id = "c2", type = "country", type_id = 1)
        vI.setValid(id = "c2", type = "country", type_id = 2)
        vI.setValid(id = "c2", type = "country", type_id = 3)
        items = vI.getValid(type = "country", type_id = 1)
        self.assertEquals(2, len(items), "after adding a second item we should get back 2")
        self.assertListEqual(items, ["c1","c2"], "wrong items are in the list")


    def testDeleteValidItems(self):
        """
        testing deletion of already stored valid items
        """
        vI = validItems()
        vI.setValid(id = "c1", type = "country", type_id = 1)
        items = vI.getValid(type = "country", type_id = 1)
        self.assertEquals(1, len(items), "the same item should not get added twice")
        #adding the item again should n

        vI.setValid(id = "c2", type = "country", type_id = 1)
        items = vI.getValid(type = "country", type_id = 1)
        self.assertEquals(2, len(items), "after adding a second item we should get back 2")
        self.assertListEqual(items, ["c1","c2"], "wrong items are in the list")

        items = vI.getValid(type = "country", type_id = 2)
        self.assertEquals(0, len(items), "after adding a second item we should get back 2")
        self.assertListEqual(items, [], "wrong items are in the list")

        vI.removeValid(id = "c1", type="country", type_id = 1)
        items = vI.getValid(type = "country", type_id = 1)
        self.assertListEqual(items, ["c2"], "wrong items are in the list")

        vI.removeValid(id = "c1", type="country", type_id = 1) #removing it a second time should have no consequences
        items = vI.getValid(type = "country", type_id = 1)
        self.assertListEqual(items, ["c2"], "wrong items are in the list")


    def testGenerateImpressionStream(self):
        """

        @return:
        """

        apiGen = APIMessageGenerator()
        impressionStream = apiGen.generateImpressionStream(nImpression=1000, domains=[1,2,3], percentageRecommend=10, percentageFeedback=1)
        print impressionStream


    def testGenerateImpressionStream_Percentages(self):
        """

        @return:
        """

        apiGen = APIMessageGenerator()
        nImpression=100
        domains=[1,2,3]
        percentageRecommend=10
        percentageFeedback=1.0
        impressionStream = apiGen.generateImpressionStream(nImpression, domains, percentageRecommend)
        recommendCount = 0
        for impression in impressionStream:
            if impression["config"]["recommend"]:
                recommendCount += 1.0
            #if impression
            print impression

        print recommendCount
        impression_ratio = nImpression*percentageRecommend/100/recommendCount
        print impression_ratio
        self.assertEqual( 0.9 < impression_ratio < 1.1, True, "the impression recommend ratio is wrong")
        self.assertEqual(1,2)


    def testProcessMessageIntegration(self):
        apiGen = APIMessageGenerator()
        impressionStream = apiGen.generateImpressionStream(nImpression=1000, domains=[1,2,3], percentageRecommend=10)
        apiGen.evaluateProcessMessage(impressionStream)
        print impressionStream