import json
import random


__author__ = 'christian'

# FIXME this is redundant to Plista Contest Emulation
class APIMessageGenerator(object):
    """ generator impression, feedback and error messages
    """

    def get_custom_impression_message(self, id_flag, client_id_flag, item_id_flag, recommendable_flag, recommend_flag, domain_id, message_json = True):
        message = {"msg":"impression",
                   "id":id_flag,
                   "client":{"id":client_id_flag},
                   "domain":{"id":domain_id},
                   "item":{"id":item_id_flag,
                           "title":"The Title Text",
                           "url":"http://url.de",
                           "created":"1318417485",
                           "text":"Mit zehn Siegen ",
                           "img":None,
                           "recommendable":recommendable_flag},
                   "config":{"timeout":None,
                             "recommend":recommend_flag,
                             "limit":4},
                   "version":"1.0"}

        if message_json:
            return json.dumps(message)
        else:
            return message


    def get_custom_feedback_message(self, domain_id, item_id_flag, client_id, message_json = True):
        """
        @param message_id: the id of message
        @param domain_id: the domain id
        @param client_id_flag: the user id
        @param source_id: the source where the user came from
        @param item_id_flag:
        @param message_json:
        @return:
        """

        message = {"msg":"feedback",
                   "client":
                       {"id":client_id},
                   "domain":{"id":domain_id},
                   "source":{"id":"2311"},
                   "target":{"id": item_id_flag},
                   "config":{"team":{"id":22}},
                   "version":"1.0"}

        if message_json:
            return json.dumps(message)
        else:
            return message


    def generateImpressionStream(self, nImpression, domains, percentageRecommend = 10):
        """
        @param nImpression: the domain we
        @type nImpression: int
        @type domains: list
        @param domains: the domain we
        @type domains: list
        @param percentageRecommend: amount of message which ask for a recommendation
        @type percentageRecommend: float
        @return: impressionStream
        """
        impressionStream = []
        # TODO create loads of impressions
        for impression in xrange(nImpression):
            random_dice = random.randint(0,100)
            recommend_flag = random_dice < percentageRecommend
            user_id = random.randint(0,100)
            id_flag = 0 # TODO needs to be overwritten later
            recommendable_flag = random.randint(0,1)
            domain_id = domains[random.randint(0,len(domains)-1)]
            item_id = self.getItemIdForDomain(domain_id)
            message = self.get_custom_impression_message(id_flag = id_flag, client_id_flag = user_id, item_id_flag = item_id,
                recommendable_flag = recommendable_flag, recommend_flag=recommend_flag, domain_id=domain_id, message_json=False)
            impressionStream.append(message)

        return impressionStream
            # TODO sometimes "recommendable" becomes reverted that should be implemented



    def overlay_feedbacks(self, impressionStream, percentageFeedback):
        """

        @param impressionStream:
        @type impressionStream: list
        @param percentageFeedback:
        @type percentageFeedback: float
        @return:
        """

        for impression in impressionStream:
            #if impression
            pass


    def evaluateProcessMessage(self, impressionStream):
        """

        @param impressionStream:
        @return:
        """
        # TODO check the validity of the items
        vI = validItems()
        for impression in impressionStream:
            item_id_recommendable = impression["item"]["recommendable"]
            if bool(item_id_recommendable):
                vI.setValid(impression["item"]["id"], "domain", impression["domain"]["id"])

        print vI.getValid(type="domain")

    def getItemIdForDomain(self, domain_id):
        """
        get an Item which is actually available on that domain
        @param domain_id:
        @return:
        """

    def setItemIdForDomain(self, domain_id):
        """
        set an Item on a domain
        @param domain_id:
        @return:
        """



class validItems(object):
    """ stores the validity of items
    """
    """{domain : {domainid : [1,2,3,4]} }

    """
    def __init__(self):
        self.dict = {}


    def setValid(self, id, type, type_id ):
        """

        @param id: the item id
        @param type: the domain or browser or ....
        @param type_id: domain_id
        @return:
        """
        # 1. try type
        # 2. try type_id
        try:
            self.dict[type]
            try:
                if not self.dict[type][type_id].__contains__(id):
                    self.dict[type][type_id].append(id)
            except:
                self.dict[type][type_id] = [id]
        except:
            self.dict[type] = { type_id : [id] }


    def getValid(self, type, type_id = None):
        if type_id == None:
            try:
                return self.dict[type]
            except: return []

        else:
            try:
                return self.dict[type][type_id]
            except: return []


    def removeValid(self, id, type, type_id):
        try:
            self.dict[type][type_id].remove(id)
            return 1
        except:
            return 0




