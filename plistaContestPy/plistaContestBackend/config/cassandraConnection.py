
"""
just a single of a cassandra instance


@author: christian.winkelmann@plista.com
"""
from plistaContestPy.plistaContestBackend.real.packages.designPatterns import Singleton
import plistaContestPy.plistaContestBackend.config


class CassandraConnection(Singleton.Singleton3):
    def __init__(self):
        pass

    def __str__(self):
        return self.s



class RedisConnection(Singleton.Singleton_Redis):
	def __init__(self):
		pass


	def __str__(self):
		return self.s


